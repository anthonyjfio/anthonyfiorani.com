---
title: Listening
layout: pages.jade
---
Podcasts
* [Criminal](http://thisiscriminal.com/)
* [99% Invisible](http://99percentinvisible.org/)
* [Love and Radio](http://loveandradio.org/)

Books
* [The Art of Communicating](http://amzn.to/1GUEFve)
* [The Willpower Instinct](http://amzn.to/1VCLkWM)
