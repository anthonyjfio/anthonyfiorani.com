---
title: Addiction
date: 2014-09-22
collection: blog
layout: blog.jade
---

Walk through a college campus and most of the students are on their cell phones. Twitter, Facebook, text message and then back to Twitter is what most of them are doing. I know because I am a student and I see it everyday I'm on campus.

Social media as we know it right now is a *huge* waste of time. People are spending a large portion of their day on their phones looking at what everyone else is doing with their lives instead of living their own. As a result they start to feel bad about themselves because they're just sitting on the couch looking at Twitter instead of out doing something.

Then on top of all the social media being used almost everyone has a cell phone and is text messaging several people at one time on it. Some people even prefer texting to phone calls, I have friends like this. This is an extremely impersonal way of communicating and I feel like it is hurting my generations social skills. On top of that most people expect you to respond within the hour and most of the time it's a conversation about nothing.

Blog on top of more blogs. There is an endless amount of information on the internet. But not all of it is necessary and chances are you really don't need to read it right now.

I'm guilty of all three of these, but I'm working on consuming less of these and creating more content. My biggest issue is reading internet articles and texting. For a long time I have been trying to cut down on text messaging and I feel that this may have caused previous intimate relationships to end which is unfourtunate. Also it makes dating women my age somewhat difficult because they're always on their phones. But I'm sure there's plenty of women out there that are on the same page as me.

These are all tools and should be used wisely. If you feel that you are over indulging on one or more of them pull the plug for a period of time. That is the reason I recently deactivated my Facebook account and Twitter might be next unless I find a way to control myself.

Our society is addicted to consuming needless information and unneccesary communication. Thus we're all just wasting time together. Join me in taking a step in the other direction.Walk through a college campus and most of the students are on their cell phones. Twitter, Facebook, text message and then back to Twitter is what most of them are doing. I know because I am a student and I see it everyday I'm on campus.

Social media as we know it right now is a HUGE waste of time. People are spending a large portion of their day on their phones looking at what everyone else is doing with their lives instead of living their own. As a result they start to feel bad about themselves because they're just sitting on the couch looking at Twitter instead of out doing something.

Then on top of all the social media being used almost everyone has a cell phone and is text messaging several people at one time on it. Some people even prefer texting to phone calls, I have friends like this. This is an extremely impersonal way of communicating and I feel like it is hurting my generations social skills. On top of that most people expect you to respond within the hour and most of the time it's a conversation about nothing.

Blog on top of more blogs. There is an endless amount of information on the internet. But not all of it is necessary and chances are you really don't need to read it right now.

I'm guilty of all three of these, but I'm working on consuming less of these and creating more content. My biggest issue is reading internet articles and texting. For a long time I have been trying to cut down on text messaging and I feel that this may have caused previous intimate relationships to end which is unfourtunate. Also it makes dating women my age somewhat difficult because they're always on their phones. But I'm sure there's plenty of women out there that are on the same page as me.

These are all tools and should be used wisely. If you feel that you are over indulging on one or more of them pull the plug for a period of time. That is the reason I recently deactivated my Facebook account and Twitter might be next unless I find a way to control myself.

Our society is addicted to consuming needless information and unneccesary communication. Thus we're all just wasting time together. Join me in taking a step in the other direction.
