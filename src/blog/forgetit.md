---
title: Forget it
date: 2015-03-10
collection: blog
layout: blog.jade
---

Learning is great, but at some point it can become an excuse to slow down or hold yourself back.

After you put in the time to learn the proper way to do something you have to forget about doing it right and just do it.

Do the thing you've been learning as fast and as hard as you can. Your time to be slow and learn has passed. It's time to get some work done.

There are times when you'll have to dial it back again just to recover. But your only doing this to recover it's not to learn. Quickly get back to your fast and furious pace. 

When you give yourself permission to forget about doing it right. You get to start learning by doing, by banging on the walls and breaking stuff. To me this is the best way I learn, and if you try it you'll be suprised to learn that this it works very well.

Make mistakes, write down what you did wrong then try again. Let your instincts guide you until you can't figure out what to do. When that happens ask for help.
