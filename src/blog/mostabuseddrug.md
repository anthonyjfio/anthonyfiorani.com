---
title: Most Abused Drug
date: 2014-10-29
collection: blog
layout: blog.jade
---

Take a guess at what it is. No, it's not caffine. That's what I thought it was at first. It's music.

Yes music, I was listening to this [video](https://www.youtube.com/watch?v=H_-zNTkYE28) made by [Tai Lopez](http://www.tailopez.com/) that brought this to my attention. But it definitely makes sense to me.

He says that the music you listen to influences the person you are and it affects your brain similar to how drugs can influence your brain.

Essentially if you listen to Kanye West every waking moment of your life your decisions are affected by that. I don't have anything against Kanye I personally like his music but I don't want him influencing my decisions.

I'd much rather fill my head with educational podcasts or books, and have those influence my decisions.

Music does have a place though, just not an large amount of it. Unless you are in the music business.

Unintentionally I have listened to very little music for the past few months. But the music I do listen to I feel that I enjoy it much more.
