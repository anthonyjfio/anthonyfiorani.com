---
title: A New First
date: 2015-08-07
collection: blog
layout: blog.jade
---

I went for a run today but it ended up turning into a half run and half walk.

But as I was getting closer to home I saw a family outside. Mother, Father, and three children. Just as I was walking by I saw one of the boys start to ride his bike for what I assume to be his first time without training wheels. His mother was running along side him trying to make sure he didn't fall.

I was glad that I got to witness such a thing. It brought a smile to my face. But it also got me thinking.

When's the last time I did something for the first time?

What comes to mind is when I went ziplineing a few weeks ago and the first time I made a "Hello World" app using a Javascript library that Facebook open sourced.

When was your last first attempt at something new?
