---
title: The More You Know
date: 2015-07-19
collection: blog
layout: blog.jade
---

I've heard this a few times over the past week and thought I would share it with all of you.

It's the saying that *the more you know, the more you can know.*

My understanding of this is that if you look at everything you know as a closet with shelves. Then every unrelated thing has it's own shelf and all related things are placed on their corresponding shelf.

So then the new unrelated things you learn will add more shelves to the existing closet. So if you keep building more shelves you'll have more space to put new clothes (ideas).

The example I keep going back to is computer programming. Let's say you learn Javascript first. That will build a new shelf in your closet. Then you learn C this will be placed ontop of the javascript shelf.

After that you decide to learn to play the harmonica, but you have musical experience from playing the saxophone in middle school. So you have an existing shelf to place learning the harmonica onto.

What are you learning?
