--- 
title: Non-acceptance
date: 2015-13-06
collection: blog
layout: blog.jade
draft: true
---

Today while I was reading [The Power of Now](http://amzn.to/1HjNbpc) the author mentions his theory on how people cause themselves pain. It was that not accepting the change in circumstances creates pain. This made a lot of sense to me and immediately I closed the book and reflected on it. I noticed situations where if I had accepted the change it would have minimized the pain I put myself through.

Now like most people I've had my fair share of heartbreaks. When I was going through them it seems that the reason I went through so much pain is because I wasn't accepting the situation. I couldn't accept that the relationship wasn't going to work out for one reason or another.

Also like most people I've caused a few hearts to break, and I've seen the same thing that I went through happen to someone else.

For me this is really the only situation I can think of where I didn't accept the circumstances and it resulted in a lot of pain. But theses were probably the most painful experiences of my life. If you can think of any situations where this applied to your life [tell me about it](mailto:anthony@anthonyfiorani.com).

