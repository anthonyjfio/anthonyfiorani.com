---
title: Drowning in Arch Sea
date: 2015-01-20
collection: blog
layout: blog.jade
---

Last Friday I made the decision to switch to Arch Linux. So I grabbed all the food and water I could find and headed to the lair.

Now let me describe the lair. This is not a place you want to be on a Friday night, it's dark and there's bugs everywhere. But I was determined to get Arch installed for the first time.

This definitely was no easy task, and the I feel like the Arch Wiki isn't good for a first time installation because they seem to list every possible configuration on the beginners guide. So it turns into more of a scramble to figure out the easiest way to get it up and running.

The only way I can describe this process is like you are drowning.

Imagine you have a laptop in front of you. It's on you can hear the humming of the fan but all you see on the screen is black and a blinking cursor. Trying to figure out how to get the machine to the point of usablity is the worst part. That's because you're so close to finishing the setup that you can smell it. But you have to search the internet for some help on this. The internet is a huge place though. 

Where do you go? Google? No, you know better than that. They'll track you down like your crazy ex-girlfriends do. 

[DuckDuckGo](https://duckduckgo.com/) will help you in this search for usability.

Eventually you stumble upon this [article](http://tutos.readthedocs.org/en/latest/source/Arch.html) which is exactly what you were looking for.

Finally you get the to a screen that isn't just a blinking cursor.

The best way I could describe the whole experience of getting Arch installed is that you feel like you're drowning, but once you get out of the water you see a whole new more beautiful world called Arch Linux.
