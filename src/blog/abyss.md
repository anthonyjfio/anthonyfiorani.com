---
title: Peer Into The Abyss
date: 2015-05-11
collection: blog
layout: blog.jade
---

About ten months ago I stumbled upon an article detailing how to make a web application. I thought it was really interesting, but ever since then everything has changed for me.

I decided to try to learn to program.

When most people think of programming they think of a guy with headphones on in front of a monitor typing ones and zeros that will somehow create something. Most people have no idea how programming works, I didn't either.

My first stop was [Codecademy](http://www.codecademy.com/). I started with HTML and CSS as those languages create websites. HTML creates the content, and CSS styles it. I like to think of them as Batman and Robin.

Shortly thereafter I started to realize how much work goes into websites most people frequent. To get an idea of this visit your favorite website and press Control and U. You'll see the HTML that creates a website.

This is just the start.

To add interactivity to websites a language called JavaScript is used. This interactivity is what creates fancy animations.

Don't even get me started on comments or social networks. I have no idea how they work, but from what I do know they're pretty complicated and I'm amazed it all works.

I'm definitely no expert but I have peered into the abyss. I've looked at the other side that most users don't see. I encourage you to do the same, maybe even start a track on Codecademy to gain some appreciation for the work that goes into your favorite websites.
