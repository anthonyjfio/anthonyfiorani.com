---
title: Frequency
date: 2014-10-27
collection: blog
layout: blog.jade
---

I mentioned before how I feel that tracking is a [lost art](alostart.html), because many people avoid it thinking it is difficult. When the results you can achieve tracking things are much better and usually you get there more efficiently. And honestly it is not that difficult, just give it a try.

But when it comes to exercising there seems to be one variable that's often over looked. Most people go into the gym trying to add weight or reps over time which will work. 

Now adding weight or reps is necessary over time to make progress. But most people do not think about increasing frequency. 

I've tried various frequency on the powerlifts (squat, bench, and deadlift) everywhere from once a week to eight times a week. I have made progress on every style that I have tried. But I enjoyed my training the most and made the best progress with a higher frequency.

Don't jump right in to training squats and bench eight times a week with the volume and intensity that you're using now. Slowly increase your frequency, and while you do this do not push your intensity to hard. Emphasize one training variable at a time.

This style of training is definitely not for everyone and I think it can be effective to change frequency every once in a while (meaning about two months). But it is definitely worth a try and it is quite fun.

Some popular powerlifters that use/have used high frequency training:

+ [Damien Pezzuti](https://www.youtube.com/user/WestsideBarbellFan)
+ [Greg Nuckols](https://www.youtube.com/user/gnuckols)
