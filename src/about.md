---
title: About
layout: pages.jade
---

I'm currently a student at [Macomb Community College](https://en.wikipedia.org/wiki/Macomb_Community_College), working towards a degree in business management. In the free time I do have I like to learn technical things such as: `HTML`, `CSS`, `Javascript`, and server optimization/management.

**Email:** [anthony@anthonyfiorani.com](mailto:anthony@anthonyfiorani.com)

**Github:** [anthonyfio](https://github.com/anthonyfio)

**Twitter:** [@anthonyjfio](https://www.twitter.com/anthonyjfio)

![Dinner in Gatlinburg Tennessee](/images/dinner.jpeg)
