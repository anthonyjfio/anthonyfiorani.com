# anthonyfiorani.com

To deply this website [zopfli](https://github.com/google/zopfli) is required. Also configure your server to serve precompressed files.

I created this website using [Metalsmith](http://www.metalsmith.io/). If you have any questions please feel free to email me [anthony@anthonyfiorani.com](mailto:anthony@anthonyfiorani.com).

### To-Do

* inline css if css + html is under 14k using [this](https://github.com/fmal/gulp-inline-source)
