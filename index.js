var Metalsmith   = require('metalsmith'),
    moment       = require('moment'),
    $            = require('load-metalsmith-plugins')();

Metalsmith(__dirname)
  .source('./src')
  .use($.ignore([
    'vendor/**',
    'vendor/**/.*',
    'css/type-settings.styl'
  ]))
  .use($.markdown())
  .use($.drafts())
  .use($.collections({
    blog: {
      pattern: './blog/*.md',
      sortBy: 'date',
      reverse: true
    }
  }))
  .use($.pagination({
    'collections.blog': {
      perPage: 5,
      first: 'index.html',
      layout: 'pagination.jade',
      path: ':num/index.html'
    }
  }))
  .use($.permalinks({
    pattern: ':collections/:title',
    relative: false
  }))
  .use($.snippet())
  .use($.layouts({
    engine: 'jade',
    moment: moment
  }))
  .use($.stylus())
  .use($.imagemin({ optimizationLevel: 9 }))
  .use($.htmlMinifier({
    removeComments: true,
    removeEmptyAttributes: true,
    removeEmptyElements: true,
    minifyCSS: true,
    minifyJS: true
  }))
  .use($.uncss({
    css: 'css/main.css',
    output: 'css/main.css',
    uncss: {
      ignore: [
        'a:link',
        'a:hover',
        'a:focus',
        'a:active',
        'a:visited',
        '.fade',
        '.fade.in',
        '.collapse',
        '.collapse.in',
        '.collapsing',
        /\.open/
      ]
    }
  }))
  .use($.cleanCss())
  .use($.autoprefixer())
  .use($.redirect({
    '/twitter': 'https://twitter.com/anthonyjfio',
    '/github': 'https://github.com/anthonyfio',
    '/gitlab': 'https://gitlab.com/u/anthonyfio'
  }))

  .destination('./build')
  .build(function(err) {
    if (err) {
      throw err;
    } else {
      console.log("Build Complete");
    }
  });
