var plan      = require('flightplan')
  , $host     = require('./.host')
  , $username = require('./.username') // Login username
  , $password = require('./.password') // Login password
  , $port     = require('./.port');    // Login port

// For username, password and port:
// * create files named `.username.js`, `.password.js` and `.port.js`
// * add `.username.js`, `.password.js` and `.port.js` to your projects
//   `.gitignore` file
//
//     EX. 
//         bower_components
//         build
//         node_modules
//         .host.js
//         .username.js
//         .password.js
//         .port.js
//
// * Now edit `.username.js` and add the text below replacing `username` with your username
//
//     EX.
//         module.exports = 'username'
//
// * Do the same with `.password.js` and `.port.js` using your password this time
// * FLY
//
//     EX.
//       $ fly server

config = {
  host: $host,
  username: $username,
  password: $password,
  port: $port,
  agent: process.env.SSH_AUTH_SOCK,
  webroot: '/home/' + $username,
  webloc: '/home/' + $username + '/' + $host
};

plan.target('server', config);

plan.local(function (local) {

  local.log('Run Build');
  local.exec('node index.js');

  local.exec('zopfli --i1000 ./build/*.html');
  local.exec('zopfli --i1000 ./build/**/*.html');
  local.exec('zopfli --i1000 ./build/**/**/*.html');
  local.exec('zopfli --i1000 ./build/css/*.css');
  local.exec('zopfli --i1000 ./build/images/*.jpeg');

  local.log('Copy files to remote hosts');
  var filesToCopy = local.exec('git ls-files -o build/', {silent: true});
  local.transfer(filesToCopy, '/tmp/' + config.host);
});

plan.remote(function (remote) {
  remote.log('Move folder');
  remote.rm('-rf ' + config.webloc);
  remote.sudo('cp -R /tmp/'+ config.host + ' ' + config.webroot, {user: $username});
  remote.exec('mv ' + config.webloc + '/build/* ' + config.webloc);
  remote.rm('-rf ' + config.webloc + '/build/');

  remote.rm('-rf /tmp/' + config.host);
});
